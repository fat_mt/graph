# graph

#### 介绍
一个对“图”数据结构进行操作的开源库，对于图的存储结构由用户进行定义，该库只将核心的、不容易变化的算法部分进行了封装，用户可以方便的进行扩展。目前只支持迪杰斯特拉最短路径算法，并将不定期更新。


以下为代码调用示例：


package pers.fat.graph;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AppTest 
    extends TestCase
{
    public void test()
    {
    	class MyNode extends Node {

			public MyNode(String nodeId) {
				super(nodeId);
			}

		}

		Node startNode = new MyNode("2");
		Node endNode = new MyNode("9");
		// 初始化图
		Graph graph = new Graph() {

			List<Node> allNodes = new ArrayList<>();
			{
				allNodes.add(new MyNode("0"));
				allNodes.add(new MyNode("1"));
				allNodes.add(new MyNode("2"));
				allNodes.add(new MyNode("3"));
				allNodes.add(new MyNode("4"));
				allNodes.add(new MyNode("5"));
				allNodes.add(new MyNode("6"));
				allNodes.add(new MyNode("7"));
				allNodes.add(new MyNode("8"));
				allNodes.add(new MyNode("9"));

			}

			int[][] edgs = new int[][] {
					new int[] { 0, 2, 3, -1, -1, -1, -1, -1, -1, -1 },
					new int[] { 2, 0, 5, 1, -1, -1, -1, -1, -1, -1 },
					new int[] { 3, 5, 0, 4, -1, -1, 2, -1, -1, -1 },
					new int[] { -1, 1, 4, 0, 3, 1, -1, -1, -1, -1 },
					new int[] { -1, -1, -1, 3, 0, -1, -1, 2, -1, -1 },
					new int[] { -1, -1, -1, 1, -1, 0, -1, 4, -1, -1 },
					new int[] { -1, -1, 2, -1, -1, -1, 0, -1, 2, -1 },
					new int[] { -1, -1, -1, -1, 2, 4, -1, 0, -1, 3 },
					new int[] { -1, -1, -1, -1, -1, -1, 2, -1, 0, 3 },
					new int[] { -1, -1, -1, -1, -1, -1, -1, 3, 3, 0 } };

			@Override
			public List<Node> getNextNodes(Node curNode) {
				List<Node> nextNodes = new ArrayList<>();
				for (int j = 0; j < edgs[Integer.valueOf(curNode.getNodeId())].length; j++) {
					int ed = edgs[Integer.valueOf(curNode.getNodeId())][j];
					if (ed > 0) {
						nextNodes.add(allNodes.get(j));
					}
				}
				return nextNodes;
			}

			@Override
			public double getWeight(Node fromNode, Node toNode) {
				return edgs[Integer.valueOf(fromNode.getNodeId())][Integer
						.valueOf(toNode.getNodeId())];
			}

		};
		// 选择最短路径算法
		ShortestPathByDijkstra dijkstra = new ShortestPathByDijkstra(graph);
		// 得到最短路径
		SWPath path = dijkstra.getShortestPath(startNode, endNode);

		System.out.println(path);
    }
}
